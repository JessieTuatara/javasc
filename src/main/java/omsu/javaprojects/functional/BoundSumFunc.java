package omsu.javaprojects.functional;

import omsu.javaprojects.arity.Function1Arity;

public class BoundSumFunc implements PolyarityFunctional {
    private double leftBound;
    private double rightBound;

    public BoundSumFunc(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    @Override
    public double functional(Function1Arity func) throws FunctionalExeption {
        if (func.getLeft()> leftBound || func.getRight()<rightBound) {
            throw new FunctionalExeption();
        }
        return func.getValueAtPoint(leftBound) + func.getValueAtPoint(rightBound) + func.getValueAtPoint((rightBound + leftBound) / 2);
    }
}
