package omsu.javaprojects.functional;

public class FunctionalExeption extends Exception {
    public FunctionalExeption() {
    }

    public FunctionalExeption(String message) {
        super(message);
    }

    public FunctionalExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public FunctionalExeption(Throwable cause) {
        super(cause);
    }
}
