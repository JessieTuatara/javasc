package omsu.javaprojects.arity;

public class Linear1Arity implements Function1Arity {
    //f(x) = Ax+b;
    private double a;
    private double b;
    private double left;
    private double right;

    public Linear1Arity(double a, double b, double left, double right) {
        this.a = a;
        this.b = b;
        if(left<right) {
            this.left = left;
            this.right = right;
        }else {
            this.left = right;
            this.right = left;
        }
    }

    public double getValueAtPoint(double x) {
        if (x > right || x < left) {
            throw new RuntimeException();
        }
        return a * x + b;
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getRight() {
        return right;
    }
}
